package project.domain;

public class Doctor extends User{
    
        public Doctor() {
        }
        public Doctor(String name, String surname, String username, Password password) {
            super(name, surname, username, password);
            setAction(treatment);
        }

        public String getAction() {
            return treatment;
        }

        public void setAction(String game) {
            this.treatment = treatment;
        }

        @Override
        public String toString() {
            return super.toString() + " " + treatment;
        }

    }
}
